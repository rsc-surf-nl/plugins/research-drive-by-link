# Research Drive by Link

**It is easy to connect SURF's Research Drive service to SURF Research Cloud.
The tried-and-true way of connecting a Research Cloud workspace with Research Drive is [documented here](https://servicedesk.surf.nl/wiki/x/OIOV).
This way of connecting Research Drive is tied to the current user's account. For some applications, however, you want the access not to be coupled to one user and their access rights. Think e.g. of a background service that writes to or reads from Research Drive. This is where Research Drive's "Public Link" feature comes in.**

## Purpose

This component creates a "Public Link"-based connection that mounts the respective Research Drive folder to a direcory on the workspace.
You won't have to reconfigure a running service when a user leaves the team or when their access rights on Research Drive change.
And certainly no user will have to share folders through the service that are not relveant to the project.

## Options

This component can automatically encrypt anything that is written through this Research Drive connection. This way, the data is only accessible from the collaboration that generated the data (or for those who own and manage the used encryption keys).

Another option is to give access to the root user only. Or to allow all users on the workspace to use this Research Drive connection.

## Usage

- In the Research Drive portal, create a public link with the access rights that are appropriate for the application. You can follow [this tutorial](https://wiki.surfnet.nl/x/5cLaAQ). Preserve the resulting URL and the chosen password somewhere safe.
- In the Research Cloud portal, create/clone an Ubuntu [catalog item](https://servicedesk.surf.nl/wiki/x/FIAHAQ) (you have to be a developer in the collaboration to do this)
- Add the "SRC External" component to the catalog item if it is not already present
- Add this "Research Drive by Link" component after the external component
- For the collaboration, set the collaboration secrets as described below
- In the catalog item, set the parameters as described below
- Start a workspace with the modified catalog item and test the connection

## Collaboration Secrets

Go to the "Profile" tab of the Research Cloud portal.
Extend the display tile of the collaboration that will use the connection.
In the tile, select the "Secrets" tab.
Add the following secrets:
- Name: **rd_by_link_url** Value: The URL of your Research Drive instance. The URL of SURF's community instance for example is "https://researchdrive.surfsara.nl/public.php/webdav/". The URL of your institute's instance will look slighly different. You can derive it from the URL you use for personal access. Please note that this URL contains **"public.php"** instead of "remote.php". Remote would be a URL for access per user, whereas "public.php" represents the created "public" link.
- Name: **rd_by_link_user** Value: The last section of the URL of the created Research Drive link. The "random string"
- Name: **rd_by_link_pass** Value: The password you chose when creating the public Research Drive link
- Name: **rd_by_link_encrypt_secret_1** Value: The first encryption key. Just use a uuid. Make sure you save it somewhere safe _before_ entering it into the collaboration secrets. Your data on Research Drive will be useless withouth this key.
- Name: **rd_by_link_encrypt_secret_2** Value: The second encryption key. Another uuid. Make sure you save it somewhere safe _before_ entering it into the collaboration secrets. Your data on Research Drive will be useless withouth this key.


## Catalog Item Parameters

When filling in the component's parameters in the catalog item you have a few choices.

#### rd_by_link_allow_all_users

_\[true|false\], default: true_

Should all users be allowed to use the connection? Or only root?

#### rd_by_link_encrypt_filenames

_\[true|false\], default: false_

Should the names of the written files be encrypted?

#### rd_by_link_encrypt_directorynames

_\[true|false\], default: false_

Should even the names of the created directories be encrypted?

#### rd_by_link_mountdir

_one simple alphanumeric string_

The directory name under ```/data/``` where the Research Drive link will be mounted.

#### rd_by_link_use_encryption

_\[true|false\], default: false_

Whether the written data gets encrypted. This will not encrypt existing files in the linked folder. 

### Implementation

This component is based on [RClone](https://rclone.org/).
